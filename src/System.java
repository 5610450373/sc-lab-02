
public class System {
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;
	private static final double INITIAL_BALANCE = 1000; 
	private static final double DEFAULT_RATE = 5;
	private BankAccount account;
	public InvestmentFrame Frame;
	
	public System(){
		
		account = new BankAccount(INITIAL_BALANCE);
		Frame = new InvestmentFrame(account.getBalance());
		Frame.createTextField(DEFAULT_RATE);
		Frame.setVisible(true);
	    Frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	    Frame.createButton(account);
	    Frame.createPanel();
	}
	public static void main(String[] args){
		   new System();
	   }
}
